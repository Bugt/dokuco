# dokuco

DigitalOcean Kubernetes Config sync


### How to use

Download latest dokuco from https://gitlab.com/hexhibit/dokuco/releases

#### Digital Ocean token

- Log into https://cloud.digitalocean.com
- Navigate to "API"
- Under "Tokens/Keys" click on "Generate New Token"
- Enter token name and choose "Read" scope only
- Generate and copy token

#### Create dokuce config

- Navigate to ~/.kube/
- create a file named "do.conf" and paste the digitalOcean API token in it

#### Use dokuce

- Run dokuce, this will create a kubeconfig file for every cluster defined in do.conf
- Use the kubeconfig file either with
  - kubectl --kubeconfig \<configFile>
  - Add the kubeconfig files to the KUBECONFIG environment variable. You can add multiple config files, seperated with ; 
  - Full documentation: https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/